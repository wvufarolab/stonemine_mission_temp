#include <iostream>
#include <string>
#include <stdio.h>
#include <unistd.h>
#include <termios.h>
#include <vector>
#include <unistd.h>
#include <ros/ros.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2/convert.h>
#include <std_msgs/Bool.h>
#include <std_msgs/String.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <state_machine/DroneMsg.h>
#include <state_machine/state_machine.hpp>


class MissionTemp
{
private:

    ros::NodeHandle nh_;

public:
    MissionTemp(ros::NodeHandle & nh);

    ros::Publisher mission_temp_pub;

    ros::Subscriber state_machine_sub;
    ros::Subscriber pose_sub;
    
    geometry_msgs::PoseStamped current_pose_;

    void statemachineCallback(const state_machine::DroneMsg::ConstPtr& msg);
    void poseCallback(const geometry_msgs::PoseStamped::ConstPtr& msg);

    void executeMissionTemp();

    const double WP_DIST = 0.2;
    const double KXY = 0.2;
    const double KZ = 0.3;

    double VelocityRamp(double last_speed, double ramp_acc, double max_speed, double min_speed);
    
    const std::vector<double> goal_takeoff_  = {0.0, 0.0, 0.8};

    std::vector<double> goal_ = goal_takeoff_;
    std::vector<double> curr_pos_ = {0.0, 0.0, 0.0};
    ros::Time timer_; 

    const double TOTAL_TIME = 2.0;
    enum PHASE{init, takeoff, moveX, approachX, moveY, approachY, moveYaw, approachYaw, land};

    int phase_ = -1;
    int counter_ = 0;
};


