#include <mission_temp/mission_temp.hpp>

MissionTemp::MissionTemp(ros::NodeHandle & nh):
 nh_(nh)
{
    // Subscribing to the State Machine
    state_machine_sub = nh_.subscribe<state_machine::DroneMsg>
            ("state_machine", 10, &MissionTemp::statemachineCallback, this);

    // Subcribing to the drone position Topic
    pose_sub = nh_.subscribe<geometry_msgs::PoseStamped>
            ("mavros/local_position/pose", 10, &MissionTemp::poseCallback, this);

    // Publisher that the node will use to publish the message 
    mission_temp_pub = nh_.advertise<state_machine::DroneMsg>
            ("command", 10);

    timer_ = ros::Time::now();
}

void MissionTemp::statemachineCallback(const state_machine::DroneMsg::ConstPtr& msg)
{
    //Check the state machine message for mission_temp, if is mission_temp, time to run the code
    if(msg->drone_state.data == DRONE_STATES::mission_temp)
    {
        ROS_INFO("Initilizing MissionTemp.");
        phase_ = PHASE::init;
        timer_ = ros::Time::now();
    }
}


void MissionTemp::poseCallback(const geometry_msgs::PoseStamped::ConstPtr& msg)
{
    // Receives the current position of the drone and run the mission
    curr_pos_ = {msg->pose.position.x, msg->pose.position.y, msg->pose.position.z};
    executeMissionTemp();
}

void MissionTemp::executeMissionTemp()
{
    state_machine::DroneMsg drone_msg;
    drone_msg.drone_state.data = DRONE_STATES::mission_temp;
    drone_msg.mission_status.data = MISSION_STATUS::in_progress;
    double dist = sqrt(pow(curr_pos_[0] - goal_[0],2) + pow(curr_pos_[1] - goal_[1],2) + pow(curr_pos_[2] - goal_[2],2));

    double vx = KXY * abs(goal_[0] - curr_pos_[0]);
    double vy = KXY * abs(goal_[1] - curr_pos_[1]);
    double vz = KZ * abs(goal_[2] - curr_pos_[2]);

    vx = (vx > 1.00) ? 1.00 : vx;
    vy = (vy > 1.00) ? 1.00 : vy;
    vz = (vz > 1.00) ? 1.00 : vz;
    
    vx = copysign(vx, (goal_[0] - curr_pos_[0]));
    vy = copysign(vy, (goal_[1] - curr_pos_[1]));
    vz = copysign(vz, (goal_[2] - curr_pos_[2]));


    switch (phase_)
    {
    case PHASE::init:
        {        
            ROS_INFO("Proceed to Takeoff Phase.");
            phase_ = PHASE::takeoff;
            goal_ = goal_takeoff_;
            drone_msg.cmd_type.data = DRONE_MODES::autotakeoff;
            drone_msg.mission_status.data = MISSION_STATUS::in_progress;
            mission_temp_pub.publish(drone_msg);
        }
        break;

    case PHASE::takeoff:
        {
            if (abs(curr_pos_[2] - goal_[2]) < WP_DIST)
            {
                ROS_INFO("Proceed to Move Phase.");
                phase_ = PHASE::moveX;
		        ros::Duration(10.0).sleep();
		        drone_msg.cmd_type.data = DRONE_MODES::velocity;
                drone_msg.velocity.linear.x = 0.0;
                drone_msg.velocity.linear.y = 0.0;
                drone_msg.velocity.linear.z = 0.0;
                drone_msg.velocity.angular.z = 0.0;
                mission_temp_pub.publish(drone_msg);
                timer_ = ros::Time::now();
            }
            else
            {
                ROS_INFO_THROTTLE(3,"Taking off.");
            }
        }
        break;

    case PHASE::moveX:
        {
            if (ros::Time::now() - timer_ > ros::Duration(TOTAL_TIME))
            {
                ROS_INFO("Proceed to Approach Phase.");
                phase_ = PHASE::approachX;
                timer_ = ros::Time::now();
            }
            else
            {
                double t = (ros::Time::now() - timer_).toSec();
                double speed = std::min(0.8 * (t/TOTAL_TIME), 0.8);
                drone_msg.cmd_type.data = DRONE_MODES::velocity;
                drone_msg.velocity.linear.x = speed;
                drone_msg.velocity.linear.y = 0.0;
                drone_msg.velocity.linear.z = 0.0;
                drone_msg.velocity.angular.z = 0.0;
                mission_temp_pub.publish(drone_msg);
                ROS_INFO_THROTTLE(3,"Moving.");
            }
        }
        break;

    case PHASE::approachX:
        {
            if (ros::Time::now() - timer_ > ros::Duration(TOTAL_TIME))
            {
                ROS_INFO("Proceed to Land Phase.");
                phase_ = PHASE::moveY;
                timer_ = ros::Time::now();
            }
            else
            {
                double t = (ros::Time::now() - timer_).toSec();
                double speed = std::min(0.8 * (t/TOTAL_TIME), 0.8);
                drone_msg.cmd_type.data = DRONE_MODES::velocity;
                drone_msg.velocity.linear.x = -speed;
                drone_msg.velocity.linear.y = 0.0;
                drone_msg.velocity.linear.z = 0.0;
                drone_msg.velocity.angular.z = 0.0;
                mission_temp_pub.publish(drone_msg);
                ROS_INFO_THROTTLE(3,"Approaching.");
            }            
        }
        break;

    case PHASE::moveY:
        {
            if (ros::Time::now() - timer_ > ros::Duration(TOTAL_TIME))
            {
                ROS_INFO("Proceed to Approach Phase.");
                phase_ = PHASE::approachY;
                timer_ = ros::Time::now();
            }
            else
            {
                double t = (ros::Time::now() - timer_).toSec();
                double speed = std::min(0.8 * (t/TOTAL_TIME), 0.8);
                drone_msg.cmd_type.data = DRONE_MODES::velocity;
                drone_msg.velocity.linear.x = 0.0;
                drone_msg.velocity.linear.y = speed;
                drone_msg.velocity.linear.z = 0.0;
                drone_msg.velocity.angular.z = 0.0;
                mission_temp_pub.publish(drone_msg);
                ROS_INFO_THROTTLE(3,"Moving.");
            }
        }
        break;

    case PHASE::approachY:
        {
            if (ros::Time::now() - timer_ > ros::Duration(TOTAL_TIME))
            {
                ROS_INFO("Proceed to Approach Phase.");
                phase_ = PHASE::moveYaw;
                timer_ = ros::Time::now();
            }
            else
            {
                double t = (ros::Time::now() - timer_).toSec();
                double speed = std::min(0.8 * (t/TOTAL_TIME), 0.8);
                drone_msg.cmd_type.data = DRONE_MODES::velocity;
                drone_msg.velocity.linear.x = 0.0;
                drone_msg.velocity.linear.y = -speed;
                drone_msg.velocity.linear.z = 0.0;
                drone_msg.velocity.angular.z = 0.0;
                mission_temp_pub.publish(drone_msg);
                ROS_INFO_THROTTLE(3,"Approaching.");
            }            
        }
        break;


    case PHASE::moveYaw:
        {
            if (ros::Time::now() - timer_ > ros::Duration(TOTAL_TIME))
            {
                ROS_INFO("Proceed to Approach Phase.");
                phase_ = PHASE::approachYaw;
                timer_ = ros::Time::now();
            }
            else
            {
                double t = (ros::Time::now() - timer_).toSec();
                double speed = std::min(1.0 * (t/TOTAL_TIME), 1.0);
                drone_msg.cmd_type.data = DRONE_MODES::velocity;
                drone_msg.velocity.linear.x = 0.0;
                drone_msg.velocity.linear.y = 0.0;
                drone_msg.velocity.linear.z = 0.0;
                drone_msg.velocity.angular.z = speed;
                mission_temp_pub.publish(drone_msg);
                ROS_INFO_THROTTLE(3,"Moving.");
            }
        }
        break;

    case PHASE::approachYaw:
        {
            if (ros::Time::now() - timer_ > ros::Duration(TOTAL_TIME))
            {
                if(counter_ < 2)
                {
                    ROS_INFO("Proceed to Approach Phase.");
                    phase_ = PHASE::moveX;
                    timer_ = ros::Time::now();
                    counter_++;
                }
                else
                {
                    ROS_INFO("Proceed to Land Phase.");
                    phase_ = PHASE::land;
                    drone_msg.cmd_type.data = DRONE_MODES::autoland;
                    drone_msg.mission_status.data = MISSION_STATUS::in_progress;
                    mission_temp_pub.publish(drone_msg);
                }
            }
            else
            {
                double t = (ros::Time::now() - timer_).toSec();
                double speed = std::min(1.0 * (t/TOTAL_TIME), 1.0);
                drone_msg.cmd_type.data = DRONE_MODES::velocity;
                drone_msg.velocity.linear.x = 0.0;
                drone_msg.velocity.linear.y = 0.0;
                drone_msg.velocity.linear.z = 0.0;
                drone_msg.velocity.angular.z = -speed;
                mission_temp_pub.publish(drone_msg);
                ROS_INFO_THROTTLE(3,"Approaching.");
            }            
        }
        break;

    case PHASE::land:
        {
            ROS_INFO("Done.");
            phase_ = -1;
            drone_msg.cmd_type.data = DRONE_MODES::autoland;
            drone_msg.mission_status.data = MISSION_STATUS::done;
            mission_temp_pub.publish(drone_msg);
        }
        break;

    default:
        break;
    }
}

int main(int argc, char **argv)
{
    // initialize ROS and the node
    ros::init(argc, argv, "MissionTemp");
    ros::NodeHandle nh;

    ros::Rate rate(50.0);

    MissionTemp mission_temp(nh);

    while(ros::ok())
    {
        ros::spinOnce();
        rate.sleep();
    }

    return 0;
}
